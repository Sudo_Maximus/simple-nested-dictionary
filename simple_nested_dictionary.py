# Из переменной "my_list" на выходе необходимо получить словарь со вложенными друг в друга элементами из списка по порядку. Рекурсию использовать нельзя.
my_list = [('a', 1), ('c', 3), ('b', 2)]


"""
Пример выходного словаря

{   
    "result": {
        "a": {
            "val": 1,
            "c": {
                "val": 3,
                "b": {
                    "val": 2
                }
            }
        }
    }
}
"""

last_dict = {}
finally_dict = {"result": last_dict}
for a in my_list:
    last_dict[a[0]] = {"val": a[1]}
    last_dict = last_dict[a[0]]

print("finally_dict", finally_dict)
